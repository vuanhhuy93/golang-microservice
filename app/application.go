package app

import (
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin"
	_ "net/http"
)

var (
	router = gin.Default()
)

func StartApplication() {
	mapUrls()
	router.Run(":9090")
}
